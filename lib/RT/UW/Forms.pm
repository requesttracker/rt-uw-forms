use warnings;
use strict;

package RT::UW::Forms;
use RT::Interface::Web;

our $VERSION = '0.99.9';

sub Fetch_CFValue {

   my ($cfname, $m, $s) = @_;

   my $queue = $m->notes('queue');
   my $ra = $m->request_args();

   my $cf = RT::CustomField->new( $s->{CurrentUser} );
   my $cfn;

   # try loading CFs for this Queue, followed by Global, followed by any CF of given $name
   $cf->LoadByName( Name => $cfname, Queue => $queue->id ) if (defined $queue);
   $cf->LoadByName( Name => $cfname, Queue => 0 ) unless ( $cf->id );
   $cf->LoadByName( Name => $cfname ) unless ( $cf->id );
   
#   my $cfn = "Object-RT::Ticket--CustomField-".$cf->id."-Values";
#   $cfn = "Object-RT::Ticket--CustomField-".$cf->id."-Value" if !defined $ra{$cfn};
#   return $ra{$cfn};

    if (RT::Interface::Web->can("GetCustomFieldInputName")) {
        $cfn = RT::Interface::Web::GetCustomFieldInputName( CustomField => $cf );
    } else {
        $cfn = "Object-RT::Ticket-" . "-CustomField-" . $cf->id .'-Value';
    }

   my $cfv =  $ra->{$cfn}||$ra->{$cfn.'s'};
   return $cfv;
}

sub Fetch_CFId {

   my ($cfname, $m, $s) = @_;
 
   my $queue = $m->notes('queue');
   my $ra = $m->request_args();
 
   my $cf = RT::CustomField->new( $s->{CurrentUser} );

   # try loading CFs for this Queue, followed by Global, followed by any CF of given $name
   $cf->LoadByName( Name => $cfname, Queue => $queue->id ) if (defined $queue);
   $cf->LoadByName( Name => $cfname, Queue => 0 ) unless ( $cf->id );
   $cf->LoadByName( Name => $cfname ) unless ( $cf->id );
 
   return $cf->id;
}

1;
